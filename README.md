# ISA Website

This is the webpage sources of 2018-2019 International Student Association of University of Oregon website(isa.uoregon.edu). The actual website might change in the future.
Author: Kenneth Liang

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## More Information
Please refer to the Final Report.pdf for more information of the construction and the progression of the project.
